import React, {Component} from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import Table from './Table';
import WaiterHeader from '../WaiterHeader';

export default class TableSelection extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader
            title="Table Selection"
            next="Menu"
            navigation={this.props.navigation}
          />
        </View>
        <ScrollView style={{flex: 1, backgroundColor: '#F7F7F7'}}>
          <View style={styles.tableWrap}>
            <Table />
            <Table />
            <Table />
            <Table />
            <Table />
            <Table />
            <Table />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tableWrap: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: RFValue(20),
  },
  tableImageWrapper: {
    width: '47%',
    height: undefined,
    aspectRatio: 1,
    backgroundColor: 'white',
    marginBottom: RFValue(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: RFValue(5),
    shadowOpacity: 1,
    elevation: 2,
  },
  tableName: {
    fontSize: RFValue(18),
  },
  tableCapacity: {
    fontSize: RFValue(15),
  },
});
