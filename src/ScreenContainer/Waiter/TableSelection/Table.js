import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  toggleSelect = () => {
    this.setState({selected: !this.state.selected});
  };
  render() {
    return (
      <TouchableOpacity
        style={styles.tableImageWrapper}
        onPress={this.toggleSelect}>
        {this.state.selected ? (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: RFValue(5),
              backgroundColor: '#FEBF11',
            }}>
            <View
              style={{
                height: '50%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                marginBottom: '5%',
              }}>
              <Image
                source={require('../../../assets/General/TableUncolored.png')}
                resizeMode="contain"
                style={{width: undefined, height: '100%', aspectRatio: 1}}
              />
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.tableName}>Table 1</Text>
              <Text style={styles.tableCapacity}>2 Persons</Text>
            </View>
          </View>
        ) : (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: RFValue(5),
            }}>
            <View
              style={{
                height: '50%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                marginBottom: '5%',
              }}>
              <Image
                source={require('../../../assets/General/TableColored.png')}
                resizeMode="contain"
                style={{width: undefined, height: '100%', aspectRatio: 1}}
              />
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.tableName}>Table 1</Text>
              <Text style={styles.tableCapacity}>2 Persons</Text>
            </View>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  tableWrap: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    padding: RFValue(20),
  },
  tableImageWrapper: {
    width: '47%',
    height: undefined,
    aspectRatio: 1,
    backgroundColor: 'white',
    marginBottom: RFValue(20),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: RFValue(5),
    shadowOpacity: 1,
    elevation: 2,
  },
  tableName: {
    fontSize: RFValue(18),
  },
  tableCapacity: {
    fontSize: RFValue(15),
  },
});
