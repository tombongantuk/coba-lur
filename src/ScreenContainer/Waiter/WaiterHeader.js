import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class WaiterHeader extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'flex-end',
          backgroundColor: 'white',
          paddingHorizontal: '5%',
        }}>
        {this.props.back ? (
          <TouchableOpacity
            style={{
              width: '15%',
              height: '50%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
            onPress={() => this.props.navigation.navigate(this.props.back)}>
            <Image
              source={require('../../assets/General/arrow.png')}
              resizeMode="contain"
              style={{width: undefined, height: '75%', aspectRatio: 1}}
            />
            <Text style={{fontSize: RFValue(15)}}>Back</Text>
          </TouchableOpacity>
        ) : (
          <View style={{width: '15%', height: '50%'}} />
        )}
        <Text style={{fontSize: RFValue(20)}}>{this.props.title}</Text>
        {this.props.next ? (
          <TouchableOpacity
            style={{
              width: '15%',
              height: '50%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
            onPress={() => this.props.navigation.navigate(this.props.next)}>
            <Text style={{fontSize: RFValue(15)}}>Next</Text>
            <Image
              source={require('../../assets/General/arrow.png')}
              resizeMode="contain"
              style={{
                width: undefined,
                height: '75%',
                aspectRatio: 1,
                transform: [{rotate: '180deg'}],
              }}
            />
          </TouchableOpacity>
        ) : (
          <View style={{width: '15%', height: '50%'}} />
        )}
      </View>
    );
  }
}
