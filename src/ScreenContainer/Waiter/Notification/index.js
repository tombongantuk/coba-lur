import React, {Component} from 'react';
import {Text, View, Image, ScrollView, TouchableOpacity} from 'react-native';
import WaiterHeader from '../WaiterHeader';
import {RFValue} from 'react-native-responsive-fontsize';
import {CheckBox} from 'react-native-elements';

import NotificationItem from './NotificationItem';

export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectAll: false,
    };
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#F7F7F7'}}>
        {console.log()}
        {console.log('Select all: ' + this.state.selectAll)}
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader title="Notification" />
        </View>
        <ScrollView style={{paddingHorizontal: '5%', marginBottom: '5%'}}>
          <View style={{width: '100%', alignItems: 'center'}}>
            <View
              style={{
                width: '100%',
                height: undefined,
                paddingHorizontal: '2%',
                aspectRatio: 10,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: 'white',
                marginBottom: RFValue(5),
                borderWidth: 0.5,
                borderColor: '#DADADA',
              }}>
              <View
                style={{
                  height: '100%',
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <CheckBox
                  containerStyle={{
                    height: '100%',
                    width: undefined,
                    aspectRatio: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  checkedIcon={
                    <Image
                      resizeMode="contain"
                      style={{
                        width: undefined,
                        height: '100%',
                        aspectRatio: 1,
                        alignSelf: 'center',
                      }}
                      source={require('../../../assets/General/checkTrue.png')}
                    />
                  }
                  uncheckedIcon={
                    <Image
                      resizeMode="contain"
                      style={{
                        width: undefined,
                        height: '100%',
                        aspectRatio: 1,
                        alignSelf: 'center',
                      }}
                      source={require('../../../assets/General/checkFalse.png')}
                    />
                  }
                  checked={this.state.selectAll}
                  onPress={() =>
                    this.setState({selectAll: !this.state.selectAll})
                  }
                />
              </View>
              <View
                style={{
                  height: '100%',
                  width: '90%',
                  justifyContent: 'center',
                }}>
                <Text style={{color: '#6A6A6A', fontSize: RFValue(14)}}>
                  Select All
                </Text>
              </View>
            </View>
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
            <NotificationItem checkedAll={this.state.selectAll} />
          </View>
        </ScrollView>
        <View
          style={{
            height: undefined,
            width: '80%',
            aspectRatio: 8,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: '#FEBF11',
            borderRadius: RFValue(5),
          }}>
          <Text style={{color: 'white', fontSize: RFValue(16)}}>
            Collect All
          </Text>
        </View>
      </View>
    );
  }
}
