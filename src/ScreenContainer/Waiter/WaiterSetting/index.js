import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import WaiterHeader from '../WaiterHeader';
import {RFValue} from 'react-native-responsive-fontsize';

export default class WaiterSetting extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader title="Settings" navigation={this.props.navigation} />
        </View>
        <View
          style={{
            height: '90%',
            paddingBottom: '10%',
            paddingHorizontal: '5%',
          }}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('WaiterEditProfile')}
            style={{
              width: '100%',
              height: undefined,
              aspectRatio: 8,
              flexDirection: 'row',
              backgroundColor: 'white',
              borderBottomWidth: 0.5,
              borderColor: '#D8D8D8',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode="contain"
                style={{height: '40%', width: undefined, aspectRatio: 1}}
                source={require('../../../assets/Waiter/Setting/person.png')}
              />
            </View>
            <Text style={{fontSize: RFValue(16)}}>Edit Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '100%',
              height: undefined,
              aspectRatio: 8,
              flexDirection: 'row',
              backgroundColor: 'white',
              borderBottomWidth: 0.5,
              borderColor: '#D8D8D8',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode="contain"
                style={{height: '40%', width: undefined, aspectRatio: 1}}
                source={require('../../../assets/Waiter/Setting/logout.png')}
              />
            </View>
            <Text style={{fontSize: RFValue(16)}}>Log Out</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
