import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import WaiterHeader from '../WaiterHeader';
import {RFValue} from 'react-native-responsive-fontsize';

export default class WaiterEditProfile extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            height: '10%',
            marginBottom: '3%',
            paddingBottom: '1%',
            backgroundColor: 'white',
            shadowOpacity: 1,
            elevation: 1,
          }}>
          <WaiterHeader
            title="Edit Profile"
            back="WaiterSetting"
            navigation={this.props.navigation}
          />
        </View>
        <View
          style={{
            height: '88%',
            backgroundColor: 'white',
            alignItems: 'center',
            paddingTop: '5%',
          }}>
          <View
            style={{
              width: '30%',
              height: undefined,
              aspectRatio: 1,
              marginBottom: '10%',
            }}>
            <Image
              resizeMode="contain"
              style={{height: '100%', width: '100%'}}
              source={require('../../../assets/General/Setting/person.png')}
            />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput style={styles.textInputStyle} placeholder="Name" />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput style={styles.textInputStyle} placeholder="Email" />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInputStyle}
              placeholder="Phone Number"
              keyboardType="phone-pad"
            />
          </View>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInputStyle}
              placeholder="Password"
              secureTextEntry={true}
            />
          </View>
          <TouchableOpacity
            style={{
              width: '70%',
              height: undefined,
              aspectRatio: 8,
              backgroundColor: '#FEBF11',
              marginTop: RFValue(25),
              borderRadius: RFValue(2),
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{color: 'white'}}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInputContainer: {
    width: '70%',
    height: undefined,
    aspectRatio: 6,
    borderBottomWidth: 0.5,
    borderColor: '#D8D8D8',
    justifyContent: 'center',
  },
  textInputStyle: {
    height: '100%',
    width: '100%',
    paddingVertical: 0,
    paddingHorizontal: '5%',
    fontSize: RFValue(16),
  },
});
