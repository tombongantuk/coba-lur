/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OrderListMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false,
    };
  }
  toggleSelect = () => {
    this.setState({selected: !this.state.selected});
  };
  render() {
    return (
      <TouchableOpacity style={styles.container}>
        <View>
          <Text>{this.props.noOrder}</Text>
        </View>
        <View>
          <Image
            source={require('../../../../assets/General/Sidebar/IconRunningOrderUnselected.png')}
            style={styles.imageStyle}
          />
        </View>
        <View>
          <Text>{this.props.noTable}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    //backgroundColor: 'red',
    paddingVertical: RFValue(4),
    marginHorizontal: RFValue(5),
  },
  imageStyle: {
    width: RFValue(55),
    height: RFValue(55),
  },
});
