import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

import OrderDetailsHeader from './OrderDetailsHeader';
import OrderDetailsOptions from './OrderDetailsOptions';
import OrderDetailsBodyComponent from './OrderDetailsBodyComponent';

export default class OrderDetailsBody extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.componentStyle}>
          <OrderDetailsHeader />
        </View>
        <View style={styles.componentStyle}>
          <OrderDetailsOptions />
        </View>
        <View style={styles.componentStyle2}>
          <OrderDetailsBodyComponent />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    //flex: 1,
    backgroundColor: '#ffffff',
    height: RFPercentage(100),
  },
  componentStyle: {
    //flex: 1,
    marginTop: RFValue(20),
    //backgroundColor: 'red',
  },
  componentStyle2: {
    //flex: 1,
    marginTop: RFValue(25),
    //backgroundColor: 'red',
  },
});
