import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import DataOrder from './dataOrder';
import {RFValue} from 'react-native-responsive-fontsize';

export default class LeftWaste extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View>
          <View style={styles.dateWrapper}>
            <Text>TODAY / Mon, March 9th 2020</Text>
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 101 - Table 5"
              itemOrder="Nasi Goreng x2, Cafe Latte..."
              time="01:01 PM"
            />
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 102 - Delivery"
              itemOrder="Mie Ayam x1"
              time="02:02 PM"
            />
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 103 - Takeaway"
              itemOrder="Americano x1"
              time="03:03 PM"
            />
          </View>
        </View>
        <View>
          <View style={styles.dateWrapper}>
            <Text>YESTERDAY / Sun, March 8th 2020</Text>
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 101 - Table 5"
              itemOrder="Nasi Goreng x2, Cafe Latte..."
              time="01.01 PM"
            />
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 102 - Delivery"
              itemOrder="Mie Ayam x1"
              time="02.02 PM"
            />
          </View>
          <View style={styles.dataContainer}>
            <DataOrder
              order="No. 103 - Takeaway"
              itemOrder="Americano x1"
              time="03.03 PM"
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    //flex: 1,
    backgroundColor: '#f5f5f5',
    paddingHorizontal: RFValue(22),
  },
  dateWrapper: {
    marginTop: RFValue(10),
  },
});
