/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
//import {NavigationEvents} from 'react-navigation';

export default class KitchenNavigator extends Component {
  render() {
    return (
      <View style={styles.outerContainer}>
        <View style={styles.inContainer}>
          <View style={styles.touchableStyle}>
            <TouchableOpacity
              style={styles.imageContainer}
              onPress={() =>
                this.props.navigation.navigate('KitchenRunningOrder')
              }>
              {this.props.RunningOrderStatus ? (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/General/Sidebar/IconRunningOrderSelected.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textSelected}>Running Order</Text>
                  </View>
                </View>
              ) : (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/General/Sidebar/IconRunningOrderUnselected.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textUnselected}>Running Order</Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.inContainer}>
          <View style={styles.touchableStyle}>
            <TouchableOpacity
              style={styles.imageContainer}
              onPress={() => this.props.navigation.navigate('CookHistory')}>
              {this.props.CookHistoryStatus ? (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/Waiter/BottomTabIcon/ActiveMenu.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textSelected}>Cook History</Text>
                  </View>
                </View>
              ) : (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/Waiter/BottomTabIcon/InactiveMenu.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textUnselected}>Cook History</Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.inContainer}>
          <View style={styles.touchableStyle}>
            <TouchableOpacity
              style={styles.imageContainer}
              onPress={() => this.props.navigation.navigate('Waste')}>
              {this.props.WasteStatus ? (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/General/Sidebar/Icon_Waste_Copy.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textSelected}>Waste</Text>
                  </View>
                </View>
              ) : (
                <View>
                  <View>
                    <Image
                      source={require('../../assets/General/Sidebar/Icon_Waste.png')}
                      style={styles.imageStyle}
                    />
                  </View>
                  <View>
                    <Text style={styles.textUnselected}>Waste</Text>
                  </View>
                </View>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touchableStyle: {
    width: '100%',
    height: '100%',
    //backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  outerContainer: {
    height: '100%',
    width: 75,
    justifyContent: 'space-around',
    alignItems: 'center',
    shadowColor: '#0000',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.5,
    shadowRadius: 2.62,
    elevation: 5,
    backgroundColor: '#ffffff',
  },
  inContainer: {
    width: '100%',
    height: '11%',
    //backgroundColor: 'red',
  },
  imageContainer: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    //alignItems: 'center',
    //backgroundColor: 'green',
  },
  imageStyle: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    //backgroundColor: 'blue',
    resizeMode: 'contain',
  },
  textSelected: {
    fontSize: 11,
    alignSelf: 'center',
    color: '#FEBF11',
    justifyContent: 'center',
  },
  textUnselected: {
    fontSize: 11,
    alignSelf: 'center',
    color: '#C5C5C5',
    justifyContent: 'center',
  },
});
