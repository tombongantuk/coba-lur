import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {RFValue, RFPercentage} from 'react-native-responsive-fontsize';

export default class Header extends Component {
  render() {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.imageWrapper}>
          <Image source={require('../../../../assets/General/back.png')} />
        </View>
        <View style={styles.textBackWrapper}>
          <Text style={styles.textBack}>Back</Text>
        </View>
        <View style={styles.textOrderDetailsWrapper}>
          <Text style={styles.textOderDetails}>ORDER DETAILS</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerContainer: {
    //flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    height: RFPercentage(8),
    width: RFPercentage(65),
    borderBottomColor: '#CCD1D1',
    borderBottomWidth: RFValue(1),
  },
  imageWrapper: {
    //backgroundColor: 'red',
    justifyContent: 'center',
  },
  textBackWrapper: {
    justifyContent: 'center',
  },
  textOrderDetailsWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
  textBack: {
    fontSize: RFValue(9),
    paddingLeft: RFValue(10),
  },
  textOderDetails: {
    alignSelf: 'flex-end',
    paddingRight: RFPercentage(20),
    fontSize: RFValue(12),
  },
});
