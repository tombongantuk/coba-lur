import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import {TextInput} from 'react-native-gesture-handler';
import {RFValue} from 'react-native-responsive-fontsize';

import TabHeader from '../Component/TabHeader.js';

export default class Profile extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: 'white',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            settingStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 1195}}>
          <TabHeader navigation={this.props.navigation} title="EDIT PROFILE" />
          <View
            style={{
              flex: 10,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: '5%',
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Image
                source={require('../../../assets/General/Setting/person.png')}
                resizeMode="contain"
                style={{width: '75%', height: undefined, aspectRatio: 1}}
              />
            </View>
            <View
              style={{
                flex: 2,
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                paddingHorizontal: '5%',
              }}>
              <TextInput
                style={{
                  width: '100%',
                  height: '8%',
                  borderBottomWidth: 1,
                  paddingVertical: 0,
                  borderColor: '#EAEAEA',
                  paddingLeft: 20,
                  marginVertical: RFValue(5),
                  fontSize: RFValue(10),
                }}
                placeholder="Name"
              />
              <TextInput
                style={{
                  width: '100%',
                  height: '8%',
                  borderBottomWidth: 1,
                  paddingVertical: 0,
                  borderColor: '#EAEAEA',
                  paddingLeft: 20,
                  marginVertical: RFValue(5),
                  fontSize: RFValue(10),
                }}
                placeholder="Email"
                keyboardType="email-address"
              />
              <TextInput
                style={{
                  width: '100%',
                  height: '8%',
                  borderBottomWidth: 1,
                  paddingVertical: 0,
                  borderColor: '#EAEAEA',
                  paddingLeft: 20,
                  marginVertical: RFValue(5),
                  fontSize: RFValue(10),
                }}
                placeholder="Password"
                secureTextEntry={true}
              />
              <TextInput
                style={{
                  width: '100%',
                  height: '8%',
                  borderBottomWidth: 1,
                  paddingVertical: 0,
                  borderColor: '#EAEAEA',
                  paddingLeft: 20,
                  marginVertical: RFValue(5),
                  fontSize: RFValue(10),
                }}
                placeholder="Confirm Password"
                secureTextEntry={true}
              />
              <View style={{height: '8%'}} />
              <TouchableOpacity
                style={{
                  width: '100%',
                  height: undefined,
                  aspectRatio: 10,
                  borderRadius: 4,
                  backgroundColor: '#FEBF11',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: 'white', fontSize: RFValue(10)}}>
                  Save
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

{
  /* <View style={{flex:10, flexDirection:'row', flex:10, paddingVertical:100}}> */
}
// <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
//     <Image
//         source={require('../../../assets/General/Setting/person.png')}
//     />
// </View>
// <View style={{flex:2, justifyContent:'center', alignItems:'center', paddingRight:100}}>
//     <TextInput
//         style={{width:'100%', borderBottomWidth:1, borderColor:'#EAEAEA', paddingLeft:20, marginVertical:10, fontSize:13.2}}
//         placeholder='Name'

//     />
//     <TextInput
//         style={{width:'100%', borderBottomWidth:1, borderColor:'#EAEAEA', paddingLeft:20, marginVertical:10, fontSize:13.2}}
//         placeholder='Email'
//         keyboardType='email-address'
//     />
//     <TextInput
//         style={{width:'100%', borderBottomWidth:1, borderColor:'#EAEAEA', paddingLeft:20, marginVertical:10, fontSize:13.2}}
//         placeholder='Password'
//         secureTextEntry={true}
//     />
//     <TextInput
//         style={{width:'100%', borderBottomWidth:1, borderColor:'#EAEAEA', paddingLeft:20, marginVertical:10, fontSize:13.2}}
//         placeholder='Confirm Password'
//         secureTextEntry={true}
//     />
//     <View style={{height:100}}/>
//     <TouchableOpacity style={{width:'100%', height:50, borderRadius:4, backgroundColor:'#FEBF11', justifyContent:'center', alignItems:'center'}}>
//         <Text style={{color:'white'}}>Save</Text>
//     </TouchableOpacity>
// </View>
