import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import CashierNavigator from '../../../Cashier/CashierNavigator';

import NoPrinter from './NoPrinter';
import AvailablePrinter from './AvailablePrinter';
import AddPrinterModal from './AddPrinterModal';
import TabHeader from '../../Component/TabHeader.js';

export default class Printer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      printerConnected: false,
      addPrinterModal: false,
    };
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: 'white',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            settingStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 1195}}>
          <TabHeader
            navigation={this.props.navigation}
            title="PRINTER SETTINGS"
          />
          <View style={{flex: 8, padding: '2%', backgroundColor: '#F7F7F7'}}>
            {this.state.printerConnected ? (
              <AvailablePrinter
                addPrinterModalSwitch={this.addPrinterModalSwitch}
              />
            ) : (
              <NoPrinter addPrinterModalSwitch={this.addPrinterModalSwitch} />
            )}
          </View>
        </View>
        <AddPrinterModal
          visible={this.state.addPrinterModal}
          addPrinterModalSwitch={this.addPrinterModalSwitch}
          printerConnectedSwitch={this.printerConnectedSwitch}
        />
      </View>
    );
  }
  addPrinterModalSwitch = () => {
    this.state.addPrinterModal
      ? this.setState({addPrinterModal: false})
      : this.setState({addPrinterModal: true});
  };
  printerConnectedSwitch = () => {
    this.addPrinterModalSwitch();
    this.setState({printerConnected: true});
  };
}
