import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class NoPrinter extends Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <Text style={{fontSize: RFValue(18)}}>
            This device has not connect yet with any printer
          </Text>
          <Image
            source={require('../../../../assets/General/Setting/printGrey.png')}
            resizeMode="contain"
            style={{width: undefined, height: '50%', aspectRatio: 1}}
          />
          <TouchableOpacity
            style={{color: '#FEBF11'}}
            onPress={this.props.addPrinterModalSwitch}>
            <Text style={{fontSize: RFValue(16), color: '#FEBF11'}}>
              Connect Printer
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
