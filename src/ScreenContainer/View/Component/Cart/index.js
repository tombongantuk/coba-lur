/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View} from 'react-native';
import CartItem from './CartItem';
import AddItemModal from '../../../Cashier/Transaction/AddItemModal';
import DeleteModal from '../DeleteModal';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addItemModalVisible: false,
      deleteModalVisible: false,
    };
  }
  addItemModalSwitch = foodName => {
    this.state.addItemModalVisible
      ? this.setState({addItemModalVisible: false, foodItem: null})
      : this.setState({addItemModalVisible: true, foodItem: foodName});
  };
  deleteModalSwitch = () => {
    this.state.deleteModalVisible
      ? this.setState({deleteModalVisible: false})
      : this.setState({deleteModalVisible: true});
  };
  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <CartItem
          quantity="1"
          foodName="Nasi Goreng"
          modifier="Telur"
          discountItem="(Discount Pelajar 20%)"
          price={30000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Done Cooking"
          isPayment={this.props.isPayment}
          press={this.addItemModalSwitch}
          longPress={this.deleteModalSwitch}
        />
        <AddItemModal
          visible={this.state.addItemModalVisible}
          addItemModalSwitch={this.addItemModalSwitch}
        />
        <DeleteModal
          visible={this.state.deleteModalVisible}
          deleteModalSwitch={this.deleteModalSwitch}
        />
        {/*<CartItem
          quantity="3"
          foodName="Mie Goreng"
          modifier="Telur"
          discountItem="(Discount Pelajar 20%)"
          price={15000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Done Cooking"
          isPayment={this.props.isPayment}
          pressAction={this.addItemModalSwitch}
        />
        <CartItem
          quantity="1"
          foodName="Mie Goreng"
          price={12000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Done Cooking"
          isPayment={this.props.isPayment}
        />
        <CartItem
          quantity="3"
          foodName="Mie Goreng"
          modifier="Telur"
          discountItem="(Discount Pelajar 20%)"
          price={15000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Cooking"
          isPayment={this.props.isPayment}
        />
        <CartItem
          quantity="3"
          foodName="Mie Goreng"
          modifier="Telur"
          discountItem="(Discount Pelajar 20%)"
          price={15000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Cooking"
          isPayment={this.props.isPayment}
        />
        <CartItem
          quantity="1"
          foodName="Mie Goreng"
          price={12000}
          isRunningOrder={this.props.isRunningOrder}
          cookStatus="Cooking"
          isPayment={this.props.isPayment}
        />*/}
      </View>
    );
  }
}
