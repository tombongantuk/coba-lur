import Login from './Login';
import RunningOrder from './RunningOrder';
import Setting from './Setting';
import Profile from './Setting/Profile';
import Printer from './Setting/Printer';

export {Login, RunningOrder, Setting, Profile, Printer};
