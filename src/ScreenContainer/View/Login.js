import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      role: 'cashier',
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
            paddingHorizontal: RFValue(150),
          }}>
          <View style={{flex: 5}} />
          <View style={{flex: 2, alignItems: 'center', width: '100%'}}>
            <Text style={{fontSize: 20}}>LOG IN USER</Text>
          </View>

          <View
            style={{
              flex: 2,
              shadowColor: '#707070',
              borderBottomColor: '#FEBF11',
              borderBottomWidth: 1,
              justifyContent: 'center',
              height: 49.48,
              width: '100%',
            }}>
            <TextInput
              placeholder="Email Address"
              style={{
                fontSize: 12,
                paddingVertical: 0,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
                width: '100%',
                alignSelf: 'center',
              }}
              placeholderTextColor={'#C6CED9'}
              onChangeText={text => this.setState({role: text})}
              keyboardType="email-address"
            />
          </View>
          <View
            style={{
              flex: 2,
              shadowColor: '#707070',
              borderBottomColor: '#FEBF11',
              borderBottomWidth: 1,
              justifyContent: 'center',
              height: 49.48,
              width: '100%',
            }}>
            <TextInput
              placeholder="Password"
              style={{
                fontSize: 12,
                paddingVertical: 0,
                color: 'black',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              placeholderTextColor={'#C6CED9'}
              onChangeText={text => this.setState({register: text})}
              secureTextEntry={true}
            />
          </View>
          <TouchableOpacity
            style={{
              width: '100%',
              height: undefined,
              aspectRatio: 11,
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              marginTop: RFValue(10),
              borderRadius: RFValue(3),
              backgroundColor: '#FEBF11',
            }}
            onPress={() => this.roleNavigate(this.state.role)}>
            <Text style={{color: 'white'}}>START</Text>
          </TouchableOpacity>
          <View style={{flex: 5}} />
        </View>
      </View>
    );
  }
  roleNavigate = role => {
    switch (role) {
      case 'cashier':
        this.props.navigation.navigate('CashierStack');
        break;
      case 'checker':
        this.props.navigation.navigate('CheckerStack');
        break;
      case 'kitchen':
        this.props.navigation.navigate('KitchenStack');
        break;
      case 'waiter':
        this.props.navigation.navigate('WaiterBottomTab');
        break;
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  imglogo: {
    width: 300,
    height: 100,
    alignItems: 'center',
    backgroundColor: '#EEEEEE',
    justifyContent: 'center',
    marginBottom: 15,
  },
});

export default Login;
