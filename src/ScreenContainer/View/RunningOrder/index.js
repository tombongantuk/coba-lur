/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LeftCashier from '../../Cashier/RunningOrder/LeftCashier';
import RightCashier from '../../Cashier/RunningOrder/RightCashier';
import LeftChecker from '../../Checker/RunningOrder/LeftChecker';
import RightChecker from '../../Checker/RunningOrder/RightChecker';

export default class RunningOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemSelected: 1,
      role: 'cashier',
    };
  }
  rightComponent = role => {
    switch (role) {
      case 'cashier':
        return <RightCashier itemSelected={this.state.itemSelected} navigation={this.props.navigation}/>;
      case 'checker':
        return <RightChecker itemSelected={this.state.itemSelected} />;
    }
  };
  leftComponent = role => {
    switch (role) {
      case 'cashier':
        return <LeftCashier selectItem={this.selectItem} />;
      case 'checker':
        return <LeftChecker selectItem={this.selectItem} />;
    }
  };

  selectItem = change => {
    this.setState({itemSelected: change});
  };

  render() {
    return (
      <View style={{flexDirection: 'row', height: '100%'}}>
        <View style={{flex: 85}}>
          {this.state.role === 'cashier' ? (
            <CashierNavigator
              runningOrderStatus={true}
              navigation={this.props.navigation}
            />
          ) : (
            <CashierNavigator
              runningOrderStatus={true}
              navigation={this.props.navigation}
            />
          )}
        </View>
        <View
          style={{
            flex: 642,
            backgroundColor: '#F7F7F7',
            borderWidth: 0.5,
            paddingHorizontal: '1%',
          }}>
          <ScrollView
            style={{width: '100%', height: '90%', marginVertical: '2%'}}>
            {this.leftComponent(this.state.role)}
          </ScrollView>
        </View>
        <View style={{flex: 555, backgroundColor: '#F7F7F7'}}>
          {this.rightComponent(this.state.role)}
        </View>
      </View>
    );
  }
}
