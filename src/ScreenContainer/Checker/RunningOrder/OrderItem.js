import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class OrderItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.selected && !this.props.selected) {
      this.setState({
        selected: nextProps.selected,
      });
    } else {
      this.setState({
        selected: nextProps.selected,
      });
    }
  }
  render() {
    return (
      <View
        style={{
          width: '100%',
          height: undefined,
          aspectRatio: 9,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '90%',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            backgroundColor: 'white',
            borderWidth: 2,
            borderColor: '#DADADA',
          }}
          onPress={() => this.setState({selected: !this.state.selected})}>
          <View
            style={{
              height: '100%',
              width: undefined,
              aspectRatio: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{height: '50%', width: '50%'}}>
              {this.state.selected ? (
                <Image
                  source={require('../../../assets/General/checkTrue.png')}
                  style={{height: '100%', width: '100%', aspectRatio: 1}}
                  resizeMode="contain"
                />
              ) : (
                <Image
                  source={require('../../../assets/General/checkFalse.png')}
                  style={{height: '100%', width: '100%', aspectRatio: 1}}
                  resizeMode="contain"
                />
              )}
            </View>
          </View>
          <View
            style={{
              height: '100%',
              width: undefined,
              flexDirection: 'row',
              alignItems: 'center',
              aspectRatio: 10,
            }}>
            <View
              style={{
                height: '100%',
                width: undefined,
                aspectRatio: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>{this.props.quantity}</Text>
            </View>
            <Text style={{fontSize: RFValue(8)}}>{this.props.item}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
