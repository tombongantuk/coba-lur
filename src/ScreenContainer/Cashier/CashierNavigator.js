/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class CashierNavigator extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'space-between',
          alignItems: 'center',
          backgroundColor: 'white',
          shadowOpacity: 1,
          shadowColor: '#C5C5C5',
          elevation: 20,
        }}>
        <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => this.props.navigation.navigate('Transaction')}>
          <View style={styles.imageContainer}>
            {this.props.cartStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconTransactionSelected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Transaction</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconTransactionUnselected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textUnselected}>Transaction</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => this.props.navigation.navigate('Activity')}>
          <View style={styles.imageContainer}>
            {this.props.activityStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconActivitySelected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Activity</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconActivityUnselected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textUnselected}>Activity</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => this.props.navigation.navigate('RunningOrder')}>
          <View style={styles.imageContainer}>
            {this.props.runningOrderStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconRunningOrderSelected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Running Order</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconRunningOrderUnselected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textUnselected}>Running Order</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableStyle}
          onPress={() => this.props.navigation.navigate('Setting')}>
          <View style={styles.imageContainer}>
            {this.props.settingStatus ? (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSettingSelected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textSelected}>Settings</Text>
              </View>
            ) : (
              <View style={styles.imageStyle}>
                <Image
                  source={require(`../../assets/General/Sidebar/IconSettingUnselected.png`)}
                  resizeMode="contain"
                  style={{width: '80%', height: undefined, aspectRatio: 1}}
                />
                <Text style={styles.textUnselected}>Settings</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  touchableStyle: {
    width: '100%',
    height: '25%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textSelected: {
    fontSize: RFValue(6),
    alignSelf: 'center',
    color: '#FEBF11',
    justifyContent: 'center',
  },
  textUnselected: {
    fontSize: RFValue(6),
    alignSelf: 'center',
    color: '#C5C5C5',
    justifyContent: 'center',
  },
});
