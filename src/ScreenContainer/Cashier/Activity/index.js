import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import CashierNavigator from '../../Cashier/CashierNavigator';

export default class Activity extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            activityStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 1195, paddingTop: '8%'}}>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              paddingHorizontal: 50,
            }}>
            <Text style={{fontSize: RFValue(18), marginBottom: '8%'}}>
              ACTIVITY
            </Text>
            <View
              style={{
                height: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity
                style={styles.cardStyle}
                onPress={() =>
                  this.props.navigation.navigate('TransactionHistory')
                }>
                <Image
                  source={require('../../../assets/Cashier/Activity/Transaction.png')}
                  resizeMode="contain"
                  style={{width: '100%', height: undefined, aspectRatio: 1}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.cardStyle}
                onPress={() => this.props.navigation.navigate('ClosingCash')}>
                <Image
                  source={require('../../../assets/Cashier/Activity/ClosingCash.png')}
                  resizeMode="contain"
                  style={{width: '100%', height: undefined, aspectRatio: 1}}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.cardStyle}
                onPress={() => this.props.navigation.navigate('ShiftHistory')}>
                <Image
                  source={require('../../../assets/Cashier/Activity/Shifting.png')}
                  resizeMode="contain"
                  style={{width: '100%', height: undefined, aspectRatio: 1}}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: undefined,
    width: '30%',
    aspectRatio: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
