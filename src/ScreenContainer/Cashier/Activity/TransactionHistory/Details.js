/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, Image, ScrollView} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class Details extends Component {
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <Text style={{fontSize: 14, fontSize: RFValue(9)}}>
          TRANSACTION DETAILS
        </Text>
        <View style={{height: '20%', marginBottom: '5%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              marginTop: '1%',
              height: '20%',
              marginBottom: '1%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: '100%',
                  width: undefined,
                  aspectRatio: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: '3%',
                }}>
                <Image
                  source={require('../../../../assets/Cashier/Activity/purse.png')}
                  resizeMode="contain"
                  style={{height: '80%', width: undefined, aspectRatio: 1}}
                />
              </View>
              <Text style={{fontSize: RFValue(9)}}>Payment Method</Text>
            </View>
            <Text style={{alignSelf: 'center', fontSize: RFValue(9)}}>
              Cash
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              marginTop: '1%',
              height: '20%',
              marginBottom: '1%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: '100%',
                  width: undefined,
                  aspectRatio: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: '3%',
                }}>
                <Image
                  source={require('../../../../assets/Cashier/Activity/money.png')}
                  resizeMode="contain"
                  style={{height: '80%', width: undefined, aspectRatio: 1}}
                />
              </View>
              <Text style={{fontSize: RFValue(9)}}>Amount Method</Text>
            </View>
            <Text style={{alignSelf: 'center', fontSize: RFValue(9)}}>
              Rp 25.000
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
              marginTop: '1%',
              height: '20%',
              marginBottom: '1%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  height: '100%',
                  width: undefined,
                  aspectRatio: 1,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginRight: '3%',
                }}>
                <Image
                  source={require('../../../../assets/Cashier/Activity/clock.png')}
                  resizeMode="contain"
                  style={{height: '80%', width: undefined, aspectRatio: 1}}
                />
              </View>
              <Text style={{fontSize: RFValue(9)}}>Time of Purchase</Text>
            </View>
            <Text style={{alignSelf: 'center', fontSize: RFValue(9)}}>
              March, 9th 2020 at 02:02 PM
            </Text>
          </View>
        </View>
        <Text style={{marginTop: '1%', fontSize: RFValue(9)}}>ITEMS</Text>
        <View
          style={{borderBottomWidth: 0.5, marginBottom: '1%'}}
          showsHorizontalScrollIndicator={false}>
          <Text style={{alignSelf: 'center', fontSize: RFValue(8)}}>
            DINE IN
          </Text>
          <View
            style={{
              paddingVertical: 10,
              marginVertical: 5,
              borderTopWidth: 0.5,
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '100%',
                height: RFValue(12),
                alignItems: 'center',
              }}>
              <Text style={{fontSize: RFValue(8)}}>Nasi Goreng</Text>
              <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginVertical: '.1%',
          }}>
          <Text style={{fontSize: RFValue(8)}}>Sub Total</Text>
          <Text style={{fontSize: RFValue(8)}}>Rp 30.000</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginVertical: '.1%',
          }}>
          <Text style={{fontSize: RFValue(8)}}>Tax</Text>
          <Text style={{fontSize: RFValue(8)}}>Rp 0</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginVertical: '.1%',
            height: '3%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              height: '100%',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(8)}}>Discount (OVO)</Text>
          </View>
          <Text
            style={{
              color: '#FEBF11',
              alignSelf: 'center',
              fontSize: RFValue(8),
            }}>
            -Rp 5.000
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
            marginVertical: '.1%',
            height: '3%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              height: '100%',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(8)}}>Total Payment</Text>
          </View>
          <Text style={{fontSize: RFValue(8), alignSelf: 'center'}}>
            Rp 25.000
          </Text>
        </View>
        <View
          style={{
            height: '10%',
            marginTop: '5%',
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={{
              height: '100%',
              width: '100%',
              borderRadius: RFValue(6),
              paddingHorizontal: '13%',
              backgroundColor: '#878787',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={this.props.changeRightComponent}>
            <Text style={{fontSize: RFValue(11), color: 'white'}}>
              ISSUE REFUND
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              height: '100%',
              width: '100%',
              borderRadius: RFValue(6),
              paddingHorizontal: '13%',
              backgroundColor: '#FEBF11',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: RFValue(11), color: 'white'}}>
              PRINT BILL
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{height: RFValue(80)}} />
      </ScrollView>
    );
  }
}
