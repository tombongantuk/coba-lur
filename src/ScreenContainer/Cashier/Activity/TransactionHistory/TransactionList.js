import React, {Component} from 'react';
import {Text, View} from 'react-native';
import TransactionItem from './TransactionItem';
import {RFValue} from 'react-native-responsive-fontsize';

export default class TransactionList extends Component {
  render() {
    return (
      <View style={{alignItems: 'center', marginBottom: RFValue(10)}}>
        <Text
          style={{
            alignSelf: 'flex-start',
            marginBottom: RFValue(5),
            fontSize: RFValue(9),
          }}>
          {this.props.date}
        </Text>
        <TransactionItem
          amount={30000}
          item="Nasi Goreng"
          time="01:02 PM"
          selectItem={this.props.selectItem}
        />
        <TransactionItem
          amount={60000}
          item="Nasi Goreng x2"
          time="12:02 AM"
          selectItem={this.props.selectItem}
        />
      </View>
    );
  }
}
