import React, {Component} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class CashDetail extends Component {
  render() {
    return (
      <View>
        <View style={{width: '100%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 10,
              paddingVertical: 2,
              borderTopWidth: 0.5,
              borderBottomWidth: 0.5,
              borderColor: '#D8D8D8',
            }}>
            <View>
              <Text style={{fontSize: RFValue(6), color: '#CBCBCB'}}>
                Friday / March, 6th 2020
              </Text>
              <Text style={{fontSize: RFValue(9.5), color: '#FEBF11'}}>
                Kas Masuk
              </Text>
            </View>
            <Text style={{fontSize: RFValue(9.5)}}>Rp 500.000</Text>
          </View>
        </View>
        <View style={{width: '100%'}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginVertical: 2,
              paddingVertical: 2,
              borderTopWidth: 0.5,
              borderBottomWidth: 0.5,
              borderColor: '#D8D8D8',
            }}>
            <View>
              <Text style={{fontSize: RFValue(6), color: '#CBCBCB'}}>
                Friday / March, 6th 2020
              </Text>
              <Text style={{fontSize: RFValue(9.5), color: '#FD734C'}}>
                Kas Keluar
              </Text>
            </View>
            <Text style={{fontSize: RFValue(9.5)}}>-Rp 120.000</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
          <TouchableOpacity
            style={{
              backgroundColor: '#FEBF11',
              borderRadius: RFValue(3),
              width: '30%',
              height: '40%',
              alignSelf: 'center',
              justifyContent: 'center',
              marginTop: 25,
            }}>
            <Text
              style={{
                color: 'white',
                alignSelf: 'center',
                fontSize: RFValue(9),
              }}>
              Rekap Kas
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
