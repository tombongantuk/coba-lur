/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {Text, View, Modal, TextInput, ScrollView} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import ModalHeader from '../../../View/Component/ModalHeader';

import Table from './Table';

export default class DineInModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.visible && !this.props.visible) {
      this.setState({
        visible: nextProps.visible,
      });
    } else {
      this.setState({
        visible: nextProps.visible,
      });
    }
  }
  render() {
    return (
      <Modal visible={this.state.visible} transparent={true}>
        <View
          style={{
            height: '100%',
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#000000aa',
          }}>
          <View
            style={{
              height: '90%',
              width: '90%',
              backgroundColor: 'white',
              alignItems: 'center',
              borderRadius: RFValue(2),
            }}>
            <View
              style={{
                height: '10%',
                width: '100%',
                shadowOpacity: 1,
                backgroundColor: 'white',
                elevation: 1,
              }}>
              <ModalHeader
                title="DINE IN"
                close={this.props.dineInModalSwitch}
                save={this.props.closeAndDineIn}
              />
            </View>

            <View style={{height: '90%', width: '100%'}}>
              <ScrollView>
                <View
                  style={{
                    width: '100%',
                    alignItems: 'center',
                    marginVertical: RFValue(15),
                  }}>
                  <View
                    style={{
                      height: RFValue(75),
                      width: '70%',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: RFValue(16)}}>Customer Name</Text>
                    <TextInput
                      style={{
                        height: RFValue(35),
                        width: '100%',
                        marginTop: RFValue(10),
                        backgroundColor: 'white',
                        borderWidth: 0.5,
                        borderRadius: 3,
                        paddingHorizontal: '7%',
                        paddingVertical: 0,
                        fontSize: RFValue(12),
                      }}
                      placeholder="Input Name"
                    />
                  </View>
                  <View
                    style={{width: '100%', width: '70%', alignItems: 'center'}}>
                    <Text style={{fontSize: RFValue(13)}}>Select Table</Text>
                    <View
                      style={{
                        alignItems: 'center',
                        width: '100%',
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                      }}>
                      <Table />
                      <Table />
                      <Table />
                      <Table />
                      <Table />
                      <Table />
                      <Table />
                    </View>
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
