/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import MenuItem from './MenuItem';

export default class LeftTransaction extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white', flexDirection: 'row'}}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#F7F7F7',
            paddingHorizontal: '3.9%',
          }}>
          <View
            style={{
              flexDirection: 'row',
              paddingBottom: 5,
              marginTop: 19,
              height: RFValue(40),
            }}>
            <View style={{width: '63.2%', marginRight: '3.2%'}}>
              <View
                style={{
                  backgroundColor: 'white',
                  flex: 1,
                  justifyContent: 'center',
                  borderRadius: 5,
                  shadowOpacity: 0.5,
                  shadowRadius: 5,
                  elevation: 3,
                }}>
                <TextInput
                  placeholder="Search..."
                  style={{fontSize: RFValue(10), justifyContent: 'center'}}
                  inputContainerStyle={{height: 30}}
                />
              </View>
            </View>
            <View
              style={{
                width: '33.5%',
                height: '100%',
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  height: '100%',
                  width: '100%',
                  backgroundColor: '#FEBF11',
                  shadowColor: '#000',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  borderRadius: RFValue(3),
                  elevation: 3,
                }}
                onPress={this.props.categoryModalSwitch}>
                <Text
                  style={{
                    alignSelf: 'center',
                    fontSize: RFValue(12),
                    color: '#FFF',
                  }}>
                  Category
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <ScrollView>
            <View
              style={{
                width: '100%',
                marginVertical: '2%',
                flexDirection: 'row',
                flexWrap: 'wrap',
              }}>
              <MenuItem
                pressAction={this.props.addItemModalSwitch}
                foodName="Nasi Goreng"
                price={25000}
              />
              <MenuItem
                pressAction={this.props.addItemModalSwitch}
                foodName="Mie Goreng"
                price={15000}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
