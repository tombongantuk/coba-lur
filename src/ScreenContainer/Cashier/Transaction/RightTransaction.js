/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from 'react-native';
import RightTransactionHeader from './RightTransactionHeader';
import {RFValue} from 'react-native-responsive-fontsize';

import InvoiceList from '../../View/Component/InvoiceList';
import InvoiceBill from '../../View/Component/InvoiceBill';
import Cart from '../../View/Component/Cart';
import AddItemModal from './AddItemModal';

export default class RightTransaction extends Component {
  render() {
    return (
      <View style={{width: undefined, height: undefined}}>
        <View
          style={{
            paddingHorizontal: '2%',
            height: '13.8%',
            backgroundColor: 'white',
          }}>
          <RightTransactionHeader
            dineInModalSwitch={this.props.dineInModalSwitch}
            isDineIn={this.props.isDineIn}
            takeawayModalSwitch={this.props.takeawayModalSwitch}
            isTakeaway={this.props.isTakeaway}
            deliveryModalSwitch={this.props.deliveryModalSwitch}
            isDelivery={this.props.isDelivery}
          />
        </View>

        <View
          style={{
            height: '86.14%',
            marginTop: '2.526%',
            backgroundColor: 'white',
          }}>
          <View style={{padding: RFValue(10), height: '53.492%'}}>
            <ScrollView>
              <Cart addItemModal={this.addItemModalSwitch} />
            </ScrollView>
          </View>
          <View
            style={{
              backgroundColor: 'white',
              justifyContent: 'space-between',
              height: '25.854%',
              paddingHorizontal: 15,
              paddingBottom: 5,
            }}>
            <InvoiceList title="Sub Total" amount={20000} />
            <InvoiceList title="Tax / Service" amount={2000} />
            <InvoiceList title="Discount" amount={11000} />
            <InvoiceBill title="Total" amount={11100} />
          </View>

          <View
            style={{
              marginTop: '2.377%',
              backgroundColor: 'white',
              paddingHorizontal: 15,
              alignContent: 'center',
              height: '14.562%',
              marginBottom: '3.715%',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                height: '100%',
              }}>
              <TouchableOpacity style={{width: '39%'}}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    backgroundColor: '#6F6F6F',
                    borderRadius: RFValue(5),
                    marginRight: 2,
                    marginBottom: 8,
                    shadowColor: '#DF2428',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 100,
                    shadowRadius: 2.62,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#6F6F6F',
                      paddingHorizontal: 10,
                      paddingVertical: 2,
                      justifyContent: 'center',
                      marginBottom: 6,
                      marginRight: 6,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(15),
                        textAlign: 'center',
                        color: '#FFFFFF',
                      }}>
                      Order
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={{width: '59%'}}
                onPress={() => this.props.navigation.navigate('Payment')}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                    backgroundColor: '#FEBF11',
                    borderRadius: RFValue(5),
                    marginRight: 2,
                    marginBottom: 8,
                    shadowColor: '#DF2428',
                    shadowOffset: {width: 0, height: 2},
                    shadowOpacity: 100,
                    shadowRadius: 2.62,
                    elevation: 3,
                  }}>
                  <View
                    style={{
                      backgroundColor: '#FEBF11',
                      paddingHorizontal: 10,
                      paddingVertical: 2,
                      justifyContent: 'center',
                      marginBottom: 6,
                      marginRight: 6,
                      borderRadius: 10,
                      flex: 1,
                    }}>
                    <Text
                      style={{
                        fontSize: RFValue(15),
                        textAlign: 'center',
                        color: '#FFFFFF',
                      }}>
                      Pay
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
