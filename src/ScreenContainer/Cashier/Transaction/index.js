/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import CashierNavigator from '../../Cashier/CashierNavigator';
import LeftTransaction from './LeftTransaction';
import RightTransaction from './RightTransaction';
import DineInModal from './DineInModal';
import TakeawayModal from './TakeawayModal';
import DeliveryModal from './DeliveryModal';
import CategoryModal from './CategoryModal';
import AddItemModal from './AddItemModal';

export default class Transaction extends Component {
    constructor(props){
        super(props);
        this.state = {
            dineInModalVisible: false,
            takeawayModalVisible:false,
            deliveryModalVisible:false,
            addItemModalVisible:false,
            isDineIn: false,
            isTakeaway: false,
            isDelivery:false,
            categoryModalVisible:false,
            foodItem:null,
        };
    }
    render() {
        return (
            <View style={{flex: 1, flexDirection: 'row'}}>
                <View style={{flex: 85, borderColor:'#DADADA', shadowOpacity: 0.5, shadowRadius: 5, borderRight: 1}}>
                    <CashierNavigator
                        cartStatus = {true}
                        navigation={this.props.navigation}
                    />
                </View>
                <View style={{flex: 640}}>
                    <LeftTransaction
                        categoryModalSwitch={this.categoryModalSwitch}
                        addItemModalSwitch={this.addItemModalSwitch}
                    />
                </View>
                <View style={{flex:555}}>
                    <RightTransaction
                        navigation={this.props.navigation}
                        dineInModalSwitch={this.dineInModalSwitch}
                        isDineIn={this.state.isDineIn}
                        takeawayModalSwitch={this.takeawayModalSwitch}
                        isTakeaway={this.state.isTakeaway}
                        deliveryModalSwitch={this.deliveryModalSwitch}
                        isDelivery={this.state.isDelivery}
                    />
                </View>
                <DineInModal
                    visible={this.state.dineInModalVisible}
                    dineInModalSwitch={this.dineInModalSwitch}
                    toggleDineIn={this.toggleDineIn}
                    closeAndDineIn={this.closeAndDineIn}
                />
                <TakeawayModal
                    visible={this.state.takeawayModalVisible}
                    takeawayModalSwitch={this.takeawayModalSwitch}
                    toggleTakeaway={this.toggleTakeaway}
                    closeAndTakeaway={this.closeAndTakeaway}
                />
                <DeliveryModal
                    visible={this.state.deliveryModalVisible}
                    deliveryModalSwitch={this.deliveryModalSwitch}
                    toggleDelivery={this.toggleDelivery}
                    closeAndDelivery={this.closeAndDelivery}
                />
                <CategoryModal
                    visible={this.state.categoryModalVisible}
                    categoryModalSwitch={this.categoryModalSwitch}
                />
                <AddItemModal
                    visible={this.state.addItemModalVisible}
                    addItemModalSwitch={this.addItemModalSwitch}
                    foodItem = {this.state.foodItem}
                />
            </View>
        );
    }
    dineInModalSwitch = () => {
        this.state.dineInModalVisible ?
        this.setState({dineInModalVisible:false}) :
        this.setState({dineInModalVisible:true});
    }
    toggleDineIn = () => {
        this.state.isDineIn ?
        this.setState({isDineIn:false}) :
        this.setState({isDineIn:true});
    }
    closeAndDineIn = () => {
        this.dineInModalSwitch();
        this.toggleDineIn();
        this.state.isDineIn = !this.state.isTakeaway ?
        null :
        this.toggleTakeaway();
        this.state.isDineIn = !this.state.isDelivery ?
        null :
        this.toggleDelivery();
    }

    takeawayModalSwitch = () => {
        this.state.takeawayModalVisible ?
        this.setState({takeawayModalVisible:false}) :
        this.setState({takeawayModalVisible:true});
    }
    toggleTakeaway = () => {
        this.state.isTakeaway ?
        this.setState({isTakeaway:false}) :
        this.setState({isTakeaway:true});
    }
    closeAndTakeaway = () => {
        this.takeawayModalSwitch();
        this.toggleTakeaway();
        this.state.isTakeaway = !this.state.isDineIn ?
        null :
        this.toggleDineIn();
        this.state.isTakeaway = !this.state.isDelivery ?
        null :
        this.toggleDelivery();
    }
    deliveryModalSwitch = () => {
        this.state.deliveryModalVisible ?
        this.setState({deliveryModalVisible:false}) :
        this.setState({deliveryModalVisible:true});
    }
    toggleDelivery = () => {
        this.state.isDelivery ?
        this.setState({isDelivery:false}) :
        this.setState({isDelivery:true});
    }
    closeAndDelivery = () => {
        console.log('Close and delivery');
        this.deliveryModalSwitch();
        this.toggleDelivery();
        this.state.isDelivery = !this.state.isDineIn ?
        null :
        this.toggleDineIn();
        this.state.isDelivery = !this.state.isTakeaway ?
        null :
        this.toggleTakeaway();
    }

    categoryModalSwitch = () => {
        this.state.categoryModalVisible ?
        this.setState({categoryModalVisible:false}) :
        this.setState({categoryModalVisible:true});
    }

    addItemModalSwitch = (foodName) => {
        this.state.addItemModalVisible ?
        this.setState({addItemModalVisible:false, foodItem:null}) :
        this.setState({addItemModalVisible:true, foodItem:foodName});
    }
}
