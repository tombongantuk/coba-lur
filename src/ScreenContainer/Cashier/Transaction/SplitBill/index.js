import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import CashierNavigator from '../../CashierNavigator';
import NumberFormat from 'react-number-format';
import {RFValue} from 'react-native-responsive-fontsize';

import CashierMiniHeader from '../../CashierMiniHeader';
import FoodItem from './FoodItem';
import ItemSplit from './ItemSplit';

export default class SplitBill extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          flexDirection: 'row',
          backgroundColor: '#F7F7F7',
        }}>
        <View style={{flex: 85}}>
          <CashierNavigator
            cartStatus={true}
            navigation={this.props.navigation}
          />
        </View>
        <View style={{flex: 1195, flexDirection: 'row'}}>
          <ScrollView style={{flex: 1, paddingVertical: RFValue(10)}}>
            <View
              style={{alignItems: 'center', paddingHorizontal: RFValue(10)}}>
              <View
                style={{alignSelf: 'flex-start', marginBottom: RFValue(10)}}>
                <Text style={{fontSize: RFValue(12)}}>No. 101 - Table 5</Text>
              </View>
              <FoodItem
                foodName="Cappucino"
                modifier="Espresso"
                price={35000}
              />
              <FoodItem
                foodName="Spaghetti Carbonara"
                modifier="Extra Sauce"
                price={75000}
              />
            </View>
          </ScrollView>
          <View style={{flex: 1.4, backgroundColor: 'white'}}>
            <View
              style={{
                flex: 51,
                paddingVertical: 10,
                backgroundColor: 'white',
                elevation: 3,
              }}>
              <CashierMiniHeader
                title="TRANSACTION DETAILS"
                navigation={this.props.navigation}
              />
            </View>
            <View style={{flex: 749}}>
              <View style={{height: '80%'}}>
                <ScrollView style={{padding: '5%'}}>
                  <View
                    style={{alignItems: 'center', paddingBottom: RFValue(50)}}>
                    <ItemSplit
                      foodName="Cappucino"
                      modifier="Espresso"
                      price={35000}
                    />
                  </View>
                </ScrollView>
              </View>
              <View
                style={{
                  height: '20%',
                  width: '100%',
                  paddingHorizontal: '5%',
                  alignItems: 'flex-end',
                }}>
                <TouchableOpacity
                  style={{
                    width: '40%',
                    height: undefined,
                    aspectRatio: 4,
                    borderRadius: RFValue(2),
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#FEBF11',
                    elevation: 4,
                  }}
                  onPress={() => this.props.navigation.navigate('Payment')}>
                  <Text style={{color: 'white'}}>PAY</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
