import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import ModifierList from './ModifierList';

export default class Modifier extends Component {
  render() {
    return (
      <View
        style={{
          width: '100%',
          alignItems: 'center',
          marginBottom: RFValue(20),
          paddingHorizontal: '10%',
        }}>
        <Text style={{fontSize: RFValue(12), marginBottom: RFValue(5)}}>
          {' '}
          SELECT MODIFIER{' '}
        </Text>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          <ModifierList name="Telur" value={2000} />
          <ModifierList name="Sambal" value={1000} />
        </View>
      </View>
    );
  }
}
