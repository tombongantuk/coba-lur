/* eslint-disable no-dupe-keys */
import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class RightTransactionHeader extends Component {
  render() {
    return (
      <View
        style={{
          height: '80%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          top: '1%',
        }}>
        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.dineInModalSwitch}>
          {this.props.isDineIn ? (
            <View style={styles.buttonStyleActive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DineInActive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleActive}>DINE IN</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DineInInactive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleInactive}>DINE IN</Text>
            </View>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.takeawayModalSwitch}>
          {this.props.isTakeaway ? (
            <View style={styles.buttonStyleActive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/TakeawayActive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleActive}>TAKE AWAY</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/TakeawayInactive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleInactive}>TAKE AWAY</Text>
            </View>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{width: '32%'}}
          onPress={this.props.deliveryModalSwitch}>
          {this.props.isDelivery ? (
            <View style={styles.buttonStyleActive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DeliveryActive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleActive}>DELIVERY</Text>
            </View>
          ) : (
            <View style={styles.buttonStyleInactive}>
              <View style={styles.imageStyle}>
                <Image
                  style={styles.icon}
                  source={require('../../../assets/Cashier/Transaction/DeliveryInactive.png')}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.textStyleInactive}>DELIVERY</Text>
            </View>
          )}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonStyleInactive: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: '#DADADA',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: 'white',
  },
  buttonStyleActive: {
    height: '100%',
    paddingHorizontal: '5%',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: '#DADADA',
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: '#FE724C',
  },
  imageStyle: {
    width: '30%',
    height: undefined,
    aspectRatio: 1,
    marginHorizontal: '5%',
  },
  icon: {
    width: '100%',
    height: undefined,
    aspectRatio: 1,
  },
  textStyleActive: {
    fontSize: RFValue(8),
    color: 'white',
  },
  textStyleInactive: {
    fontSize: RFValue(8),
  },
});
