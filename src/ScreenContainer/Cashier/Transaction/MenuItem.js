/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import NumberFormat from 'react-number-format';

export default class MenuItem extends Component {
  render() {
    return (
      <View
        style={{
          width: '25%',
          height: undefined,
          aspectRatio: 1.1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <TouchableOpacity
          style={{
            height: '90%',
            width: '90%',
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderRadius: RFValue(3),
            elevation: 3,
          }}
          onPress={() => this.props.pressAction(this.props.foodName)}>
          <View style={{height: '20%', justifyContent: 'center'}}>
            <Text style={{fontSize: RFValue(8)}}>{this.props.foodName}</Text>
          </View>
          <View
            style={{width: '100%', height: '55%', backgroundColor: 'white'}}>
            <Image
              source={require('../../../assets/Cashier/Transaction/menu.jpg')}
              resizeMode="cover"
              style={{width: '100%', height: '100%'}}
            />
          </View>
          <View style={{height: '20%', justifyContent: 'center'}}>
            <NumberFormat
              value={this.props.price}
              displayType={'text'}
              thousandSeparator={true}
              prefix={'Rp '}
              renderText={value => (
                <Text style={{fontSize: RFValue(7)}}>{value}</Text>
              )}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
