import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class SplitPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.selected,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.selected && !this.props.selected) {
      this.setState({
        selected: nextProps.selected,
      });
    } else {
      this.setState({
        selected: nextProps.selected,
      });
    }
  }
  render() {
    return (
      <TouchableOpacity
        style={{width: '45%', height: undefined, aspectRatio: 0.9}}
        onPress={this.props.selection}>
        {this.state.selected ? (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#FEBF11',
            }}>
            <View
              style={{
                width: '90%',
                height: undefined,
                aspectRatio: 1,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../../../../assets/Cashier/Transaction/SplitMethod/SplitPaymentActive.png')}
                resizeMode="contain"
                style={{width: '50%', height: undefined, aspectRatio: 1}}
              />
              <Text style={{fontSize: RFValue(15)}}>Split Payment</Text>
            </View>
            <Text style={{fontSize: RFValue(7)}}>
              Using multiple payment method for paying 1 bill
            </Text>
          </View>
        ) : (
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              elevation: 2,
            }}>
            <View
              style={{
                width: '90%',
                height: undefined,
                aspectRatio: 1,
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
              <Image
                source={require('../../../../../assets/Cashier/Transaction/SplitMethod/SplitPaymentInactive.png')}
                resizeMode="contain"
                style={{width: '50%', height: undefined, aspectRatio: 1}}
              />
              <Text style={{fontSize: RFValue(15)}}>Split Payment</Text>
            </View>
            <Text style={{fontSize: RFValue(7)}}>
              Using multiple payment method for paying 1 bill
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}
