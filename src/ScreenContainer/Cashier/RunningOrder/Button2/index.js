/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Button2 extends Component {
  render() {
    return (
      <View>
        <TouchableOpacity
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 10,
            backgroundColor: '#FEBF11',
            borderRadius: 6.6,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            this.props.navigation.navigate('RunningOrder');
          }}>
          <Text style={{color: 'white', fontSize: RFValue(10)}}>
            {this.props.text}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
