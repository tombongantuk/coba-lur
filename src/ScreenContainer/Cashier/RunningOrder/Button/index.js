/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Button extends Component {
  render() {
    return (
      <View style={{height: '100%', width: '100%', justifyContent: 'center'}}>
        <View
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 12,
            marginBottom: RFValue(3),
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              width: '49.5%',
              height: '100%',
              backgroundColor: '#878787',
              borderRadius: 6.6,
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}
            onPress={() => {
              this.props.navigation.navigate('KitchenStatus');
            }}>
            <View
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: '10%',
              }}>
              <Image
                source={require('../../../../assets/Cashier/RunningOrder/loading.png')}
                resizeMode="contain"
                style={{width: undefined, height: '80%', aspectRatio: 1}}
              />
            </View>
            <Text style={{color: 'white', fontSize: RFValue(8)}}>
              Kitchen Status
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '49.5%',
              height: '100%',
              backgroundColor: '#878787',
              borderRadius: 6.6,
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}>
            <View
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: '10%',
              }}>
              <Image
                source={require('../../../../assets/Cashier/RunningOrder/invoice.png')}
                resizeMode="contain"
                style={{width: undefined, height: '80%', aspectRatio: 1}}
              />
            </View>
            <Text style={{color: 'white', fontSize: RFValue(8)}}>
              Print Order
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 12,
            marginBottom: RFValue(3),
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            style={{
              width: '49.5%',
              height: '100%',
              backgroundColor: '#878787',
              borderRadius: 6.6,
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}
            onPress={() => {
              this.props.navigation.navigate('Transaction');
            }}>
            <View
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: '10%',
              }}>
              <Image
                source={require('../../../../assets/Cashier/RunningOrder/edit.png')}
                resizeMode="contain"
                style={{width: undefined, height: '80%', aspectRatio: 1}}
              />
            </View>
            <Text style={{color: 'white', fontSize: RFValue(8)}}>
              Edit Order
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '49.5%',
              height: '100%',
              backgroundColor: '#878787',
              borderRadius: 6.6,
              flexDirection: 'row',
              paddingHorizontal: '3%',
              alignItems: 'center',
            }}
            onPress={this.props.cancelModalSwitch}>
            <View
              style={{
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: '10%',
              }}>
              <Image
                source={require('../../../../assets/Cashier/RunningOrder/cancel.png')}
                resizeMode="contain"
                style={{width: undefined, height: '80%', aspectRatio: 1}}
              />
            </View>
            <Text style={{color: 'white', fontSize: RFValue(8)}}>
              Cancel Order
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={{
            width: '100%',
            height: undefined,
            aspectRatio: 12,
            backgroundColor: '#FEBF11',
            borderRadius: 6.6,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            this.props.navigation.navigate('Payment');
          }}>
          <Text style={{color: 'white', fontSize: RFValue(10)}}>Pay</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
